# README #

This is the repository for The Amazing Hotel rate API. 

### Structure of the Project ###

* API - Publicly exposed Web API for pulling down pricing information for a room.
* Core - Class library which holds business logic and data access. (The API adds this as a reference)
* Database - SSDT database project which holds the sql scripts for Rates and DailyRates tables and also seed data
* Test - Tests for the API endpoint as well the repository method in the core for retrieving rates

### Running the Code ###

* Running the API project should automatically open a browser to the rates endpoint with sample data, adjust the start and end dates to get different data (Note: you must publish the database by opening Database.publish.xml in the Database project and publishing it to a local SQL Server)
* Running the unit tests does not rely on published data to a local SQL Server as it uses an in memory connection to a SQLite database. This makes it very easy to test as close to the real thing on a build server without needing SQL Server configured somewhere.

### Design Notes/Thoughts ###

* You'll notice that the RatesController contains no error handling or model state validation. Do not fear! Common validation and exception handling has been moved out to action filters to keep the controller actions clean.
* I decided to return rate information for each day in the date range specified as I didn't think any consumers of the API needed to be concerned with the fact that the rates are the same for each week of the year. If the back end decided to make pricing more flexible (specify a different rate for each day of the year) then the clients wouldn't need to change. The downside to this is that we are potentially sending down a lot more data than is needed, but that also depends on if the client is trying to pull down the entire rate schedule to present pricing for just a few days or if they only ask for what they need.
* The API start/end dates assume that you only ask for data between 1/1/2017 and 12/31/2017. Dates outside of these ranges is not supported or handled.