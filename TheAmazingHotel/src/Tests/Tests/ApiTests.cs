﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using API.Helpers;
using Core;
using Test.Helpers;
using Xunit;

namespace Test.Tests
{
    public class ApiTests : IClassFixture<ApiTestFixture>
    {
        private readonly HttpClient _httpClient;

        //common test data
        public static DateTime StartDate = new DateTime(2017, 1, 1);
        public static DateTime EndDate = new DateTime(2017, 12, 31);

        public ApiTests()
        {
            _httpClient = ApiTestFixture.HttpClient;
        }

        [Fact]
        public async Task TestWithInvalidDateRange()
        {
            var startDateQuery = WebUtility.UrlEncode(StartDate.ToShortDateString());
            var endDateQuery = WebUtility.UrlEncode(EndDate.ToShortDateString());

            var url = $"/api/rates/999?startDate={endDateQuery}&endDate={startDateQuery}";
            var response = await _httpClient.GetAsync(url);

            //got correct response
            Assert.True(response.StatusCode == HttpStatusCode.BadRequest);
            Assert.NotNull(response.Content);

            //got validation errors
            var validationErrors =
                new JavaScriptSerializer().Deserialize<ValidationErrorResponse>(
                    await response.Content.ReadAsStringAsync());
            Assert.NotNull(validationErrors);
        }

        [Fact]
        public async Task TestWithMissingDateRange()
        {
            var url = "/api/rates/999";
            var response = await _httpClient.GetAsync(url);

            //got the correct status code
            Assert.True(response.StatusCode == HttpStatusCode.BadRequest);

            //got validation errors
            var validationErrors =
                new JavaScriptSerializer().Deserialize<ValidationErrorResponse>(
                    await response.Content.ReadAsStringAsync());
            Assert.NotNull(validationErrors);

            //should be two of them
            Assert.True(validationErrors.ValidationErrors.Length == 2);

        }

        [Fact]
        public async Task TestResponseWithInvalidId()
        {
            var startDate = WebUtility.UrlEncode(StartDate.ToShortDateString());
            var endDate = WebUtility.UrlEncode(EndDate.ToShortDateString());

            var url = $"/api/rates/998?startDate={startDate}&endDate={endDate}";
            var response = await _httpClient.GetAsync(url);

            Assert.True(response.StatusCode == HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestWithValidDataSucceeds()
        {
            //use a 12 day stay period
            var startDate = WebUtility.UrlEncode(StartDate.ToShortDateString());
            var endDate = WebUtility.UrlEncode(StartDate.AddDays(12).ToShortDateString());

            var url = $"/api/rates/999?startDate={startDate}&endDate={endDate}";
            var response = await _httpClient.GetAsync(url);
            //validate response state
            Assert.True(response.StatusCode == HttpStatusCode.OK);
            Assert.True(response.Content.Headers.ContentLength > 0);

            var rate =
                new JavaScriptSerializer().Deserialize<Rate>(
                    await response.Content.ReadAsStringAsync());

            //validate the room data
            Assert.NotNull(rate);
            Assert.True(rate.Id == 999);
            Assert.True(rate.Name == "King Room");

            //validate the rate data
            Assert.NotNull(rate.Rates);
            Assert.True(rate.Rates.Count() == 13);
            var lastDateRate = rate.Rates.Last();
            Assert.True(lastDateRate.Date == new DateTime(2017, 1, 13).Date);
            Assert.True(lastDateRate.MinTotalStayInDays == 2);
            Assert.True(lastDateRate.Price == (decimal)99.99);
        }
    }
}
