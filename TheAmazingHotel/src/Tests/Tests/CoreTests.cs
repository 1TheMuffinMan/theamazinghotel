﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core;
using Core.Repository;
using Xunit;

namespace Test.Tests
{
    public class CoreTests : IClassFixture<RateRepositoryTestFixture>
    {
        private readonly IRatesRepository _ratesRepository;

        public CoreTests()
        {
            _ratesRepository = RateRepositoryTestFixture.RatesRepository;
        }

        [Fact]
        public async Task TestGetRatesWithValidData()
        {
            var rate = await _ratesRepository.GetRoomRatesById(999, new DateTime(2017, 1, 1), new DateTime(2017, 1, 13));
            Assert.NotNull(rate);
            Assert.True(rate.Id == 999);
            Assert.True(rate.Name == "King Room");
            Assert.NotNull(rate.Rates);
            Assert.True(rate.Rates.Count() == 13);
        }

        [Fact]
        public void TestGetRatesWithInvalidRange()
        {
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(
                () => _ratesRepository.GetRoomRatesById(999, new DateTime(2017, 12, 31), new DateTime(2017, 1, 1)));
        }

        [Fact]
        public async Task TestGetRatesWithInvalidRoomId()
        {
            var result = await _ratesRepository.GetRoomRatesById(201, new DateTime(2017, 1, 1), new DateTime(2017, 1, 13));
            Assert.Null(result);
        }
    }
}
