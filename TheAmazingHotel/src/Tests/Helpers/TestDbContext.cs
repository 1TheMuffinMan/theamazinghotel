﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AsyncPoco;
using Core;

namespace Test.Helpers
{
    public class TestDbContext : IDbContext
    {
        private static Database _db;
        public async Task<IDatabase> GetDatabaseAsync()
        {
            if (_db == null)
            {
                var conn = new SQLiteConnection("Data Source=:memory:");
                await conn.OpenAsync();
                _db = new Database(conn);

                //create tables
                await _db.ExecuteAsync(GetSqlScript("Rates.sql"));
                await _db.ExecuteAsync(GetSqlScript("DailyRates.sql"));

                //insert data
                var ratesSql = @"INSERT INTO Rates(RateId, Name, StartDate, EndDate) VALUES(999, 'King Room', '2017-01-01', '2017-12-31')";
                await _db.ExecuteAsync(ratesSql);

                var dailyRatesSql = @"
                            INSERT INTO DailyRates(DailyRateId, RateId, [Day], Price, MinimumStay) VALUES(1, 999, 0, 59.99,1);
                            INSERT INTO DailyRates(DailyRateId, RateId, [Day], Price, MinimumStay) VALUES(2, 999, 1, 39.99, 1);
                            INSERT INTO DailyRates(DailyRateId, RateId, [Day], Price, MinimumStay) VALUES(3, 999, 2, 59.99, 1);
                            INSERT INTO DailyRates(DailyRateId, RateId, [Day], Price, MinimumStay) VALUES(4, 999, 3, 59.99, 1);
                            INSERT INTO DailyRates(DailyRateId, RateId, [Day], Price, MinimumStay) VALUES(5, 999, 4, 59.99, 1);
                            INSERT INTO DailyRates(DailyRateId, RateId, [Day], Price, MinimumStay) VALUES(6, 999, 5, 99.99, 2);
                            INSERT INTO DailyRates(DailyRateId, RateId, [Day], Price, MinimumStay) VALUES(7, 999, 6, 99.99, 1);
                            ";
                await _db.ExecuteAsync(dailyRatesSql);
            }
            else
            {
                await _db.OpenSharedConnectionAsync();
            }

            return _db;
        }

        /// <summary>
        /// Gets a sql script from the database project and sanitizes it for sqlite
        /// </summary>
        private string GetSqlScript(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            if (Path.GetExtension(name) == "")
                throw new ArgumentException($"The argument \"{nameof(name)}\" should contain a file extension such as .sql");

            var path = Path.Combine(GetParent(System.AppDomain.CurrentDomain.BaseDirectory, "TheAmazingHotel"), "Database", name);
            var file = File.ReadAllText(path);

            file = file.Replace("[dbo].", "");
            file = file.Replace("INT NOT NULL PRIMARY KEY IDENTITY", "INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT");
            file = file.Replace("\r\nCREATE", "\r\n;CREATE");
            file = file.Replace("GO", ";");

            return file;
        }

        public string GetParent(string path, string parentName)
        {
            var dir = new DirectoryInfo(path);

            if (dir.Parent == null)
            {
                return null;
            }

            if (dir.Parent.Name == parentName)
            {
                return dir.Parent.FullName;
            }

            return this.GetParent(dir.Parent.FullName, parentName);
        }
    }
}
