using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace Test.Helpers
{
    public class ApiTestFixture
    {
        public static HttpClient HttpClient { get; private set; }

        private static readonly object Lockobj = new object();
        public ApiTestFixture()
        {
            lock (Lockobj)
            {
                if (HttpClient == null)
                {
                    var service = new TestServer(new WebHostBuilder().UseStartup<Startup>());
                    HttpClient = service.CreateClient();

                }
            }
        }
    }
}
