﻿using API.Helpers;
using AutoMapper;
using Core;
using Core.Repository;
using Test.Helpers;

namespace Test
{
    public class RateRepositoryTestFixture
    {
        public static IRatesRepository RatesRepository { get; private set; }
        public RateRepositoryTestFixture()
        {
            RatesRepository = new RatesRepository(new TestDbContext(), AutoMapperConfiguration.Configure());
        }
    }
}
