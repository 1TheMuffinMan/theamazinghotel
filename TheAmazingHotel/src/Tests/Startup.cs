﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Helpers;
using Core;
using Core.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore;
using SimpleInjector.Integration.AspNetCore.Mvc;
using Test.Helpers;
using Test.Mocks;

namespace Test
{
    public class Startup
    {
        public static string ConnectionString { get; private set; }
        private static Container _container;

        public static Container Container => _container ?? (_container = new Container());

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
           // ConnectionString = Configuration.GetConnectionString("DefaultDatabase");
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(
                builder =>
                    builder.AddPolicy("default",
                        policy => policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().Build()));
            // Add framework services.
            services.AddMvc();
            services.AddSingleton<IControllerActivator>(new SimpleInjectorControllerActivator(Container));
            services.AddSingleton<IViewComponentActivator>(new SimpleInjectorViewComponentActivator(Container));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("default");
            app.UseSimpleInjectorAspNetRequestScoping(Container);
            Container.Options.DefaultScopedLifestyle = new AspNetRequestLifestyle();
            Container.RegisterSingleton(env);
            InitializeContainer(app);

            Container.Verify();
            loggerFactory.AddDebug();

            app.UseMvc();
        }

        private void InitializeContainer(IApplicationBuilder app)
        {
            // Add application presentation components:
            Container.RegisterMvcControllers(app);
            Container.RegisterMvcViewComponents(app);
            Container.RegisterSingleton<IDbContext, TestDbContext>();
            Container.RegisterSingleton(AutoMapperConfiguration.Configure);
            Container.RegisterSingleton<IRatesRepository, RatesRepositoryMock>();
        }
    }
}
