﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Repository;

namespace Test.Mocks
{
    public class RatesRepositoryMock : IRatesRepository
    {
        public Task<Rate> GetRoomRatesById(int id, DateTime startDate, DateTime endDate)
        {
            //assuming here that startdate and enddate are 1/1-1/12/17
            if (id != 999) return Task.FromResult((Rate)null);

            var rate = new Rate
            {
                Id = id,
                Name = "King Room",
                Rates = new List<DailyRate>()
            };

            for (int i = 0; i <= 12; i++)
            {
                var date = startDate.AddDays(i);
                ((List<DailyRate>)rate.Rates).Add(new DailyRate
                {
                    Date = date,
                    Price = Pricing[(int)date.DayOfWeek],
                    MinTotalStayInDays = date.DayOfWeek == DayOfWeek.Friday ? 2 : 1
                });
            }

            return Task.FromResult(rate);
        }

        private static Dictionary<int, decimal> Pricing = new Dictionary<int, decimal>
        {
            {0, 59.99m },
            {1, 39.99m },
            {2, 59.99m },
            {3, 59.99m },
            {4, 59.99m },
            {5, 99.99m },
            {6, 99.99m },
        };
    }
}
