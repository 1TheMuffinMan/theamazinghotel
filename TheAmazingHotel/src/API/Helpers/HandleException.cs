﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace API.Helpers
{
    /// <summary>
    /// Keeps controller actions clean by not requiring common try/catches
    /// </summary>
    public class HandleException : TypeFilterAttribute
    {
        public HandleException() : base(typeof(HandleExceptionImpl))
        { }

        public class HandleExceptionImpl : ExceptionFilterAttribute
        {
            private readonly IHostingEnvironment _hostingEnv;

            public HandleExceptionImpl(IHostingEnvironment hostingEnv)
            {
                _hostingEnv = hostingEnv;
            }

            public override void OnException(ExceptionContext context)
            {
                if (_hostingEnv.IsDevelopment())
                {
                    context.ExceptionHandled = true;
                    context.Result = new ObjectResult(new
                    {
                        message = context.Exception.Message,
                        stackTrace = context.Exception.StackTrace
                    })
                    { StatusCode = 500 };

                    return;
                }
            }
        }
    }
}