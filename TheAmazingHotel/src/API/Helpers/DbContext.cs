﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using AsyncPoco;
using Core;

namespace API.Helpers
{
    public class DbContext : IDbContext
    {
        public Task<IDatabase> GetDatabaseAsync()
        {
            return Task.FromResult((IDatabase)new Database(Startup.ConnectionString, "System.Data.SqlClient"));
        }
    }
}
