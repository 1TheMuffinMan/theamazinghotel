﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Helpers
{
    /// <summary>
    /// Place on the start date property to validate the date range between start and end date
    /// </summary>
    public class GetRatesStartEndDateValidator : ValidationAttribute
    {
        protected override ValidationResult
                IsValid(object value, ValidationContext validationContext)
        {
            var model = (Models.GetRatesRequest)validationContext.ObjectInstance;
            var EndDate = model.EndDate;
            var StartDate = Convert.ToDateTime(value);

            if (StartDate > EndDate)
            {
                string errorMessage = ErrorMessage ?? "The start date must come before the end date";
                return new ValidationResult
                    (errorMessage);
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}
