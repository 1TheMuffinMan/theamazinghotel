﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace API.Helpers
{
    public class ValidationErrorResponse
    {
        public ValidationError[] ValidationErrors { get; set; }
    }

    public class ValidationError
    {
        public string Field { get; set; }
        public string[] Error { get; set; }
    }

    /// <summary>
    /// Automatically responds with validation messages if ModelState validation fails. Additionally validates the required attribute placed on a
    /// parameter directly in the actionmethod
    /// </summary>
    public class ValidateModelStateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //validate attributes placed on action method paramemters themselves
            var descriptor = context.ActionDescriptor as ControllerActionDescriptor;
            if (descriptor != null)
            {
                var parameters = descriptor.MethodInfo.GetParameters();

                foreach (var parameter in parameters)
                {
                    var arg = context.ActionArguments.SingleOrDefault(x => x.Key == parameter.Name);
                    var argumentValue = arg.Equals(new KeyValuePair<string, object>())
                        ? null : arg.Value;

                    EvaluateValidationAttributes(parameter, argumentValue, context.ModelState);
                }
            }

            base.OnActionExecuting(context);

            if (!context.ModelState.IsValid)
            {
                var validationErrors = context.ModelState.Where(x=> x.Value.ValidationState == ModelValidationState.Invalid).ToDictionary(
                    kvp => kvp.Key,
                    kvp => kvp.Value.Errors.Where(x=> x != null).Select(x => x.ErrorMessage)
                    ).Select(x => new ValidationError
                    {
                        Field = x.Key,
                        Error = x.Value.ToArray()
                    }).ToArray();

                var objectRes = new ObjectResult(new ValidationErrorResponse { ValidationErrors = validationErrors })
                {
                    StatusCode = (int)HttpStatusCode.BadRequest
                };

                //flush the response
                context.Result = objectRes;
            }
        }

        private void EvaluateValidationAttributes(ParameterInfo parameter, object argument, ModelStateDictionary modelState)
        {
            var validationAttributes = parameter.CustomAttributes;

            foreach (var attributeData in validationAttributes)
            {
                var attributeInstance = CustomAttributeExtensions.GetCustomAttribute(parameter, attributeData.AttributeType);

                var validationAttribute = attributeInstance as ValidationAttribute;

                if (validationAttribute != null)
                {
                    var isValid = validationAttribute.IsValid(argument);
                    if (!isValid)
                    {
                        modelState.AddModelError(parameter.Name, validationAttribute.FormatErrorMessage(parameter.Name));
                    }
                }
            }
        }
    }
}
