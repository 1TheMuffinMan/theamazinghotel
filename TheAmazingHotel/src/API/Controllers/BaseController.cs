﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [HandleException]
    [ValidateModelState]
    public class BaseController : Controller
    {
    }
}
