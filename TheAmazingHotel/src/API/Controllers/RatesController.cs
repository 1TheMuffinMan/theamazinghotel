﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using API.Helpers;
using API.Models;
using Core;
using Core.Repository;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class RatesController : BaseController
    {
        private readonly IRatesRepository _ratesRepository;

        public RatesController(IRatesRepository ratesRepository)
        {
            _ratesRepository = ratesRepository;
        }

        /// <summary>
        /// Gets the daily rates for a particular room
        /// </summary>
        /// <param name="Id">The id of the room to get rates for</param>
        /// <param name="startDate">The start date in which to return rates</param>
        /// <param name="endDate">The end date in which to return rates</param>
        [HttpGet]
        [Route("{id:int}")]
        [ProducesResponseType(typeof(Rate), 200)]
        [ProducesResponseType(typeof(ValidationErrorResponse), 400)]
        public async Task<IActionResult> GetRatesForRoom(int id, [FromQuery]GetRatesRequest request)
        {
            //model state validation and error handling has been moved out into action filters
            var rate = await _ratesRepository.GetRoomRatesById(id, request.StartDate.Value, request.EndDate.Value);
            if (rate == null)
                return NotFound("The requested room does not exist.");

            return Ok(rate);
        }
    }
}
