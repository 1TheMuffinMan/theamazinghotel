﻿MERGE Rates AS target
	USING (VALUES
		(999, 'King Room', '2017-01-01', '2017-12-31')
		) AS source (RateId, Name, StartDate, EndDate)
		ON target.RateId = source.RateId
	WHEN MATCHED THEN
		UPDATE SET target.Name = source.Name, target.StartDate = source.StartDate, target.EndDate = source.EndDate
	WHEN NOT MATCHED THEN
		INSERT (RateId, Name, StartDate, EndDate) VALUES (source.RateId, source.Name, source.StartDate, source.EndDate);