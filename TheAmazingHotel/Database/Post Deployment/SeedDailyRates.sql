﻿SET IDENTITY_INSERT DailyRates ON
MERGE DailyRates AS target
	USING (VALUES
		(1, 999, 0, 59.99,1),  --Sun
		(2, 999, 1, 39.99, 1), --Mon
		(3, 999, 2, 59.99, 1), --Tues
		(4, 999, 3, 59.99, 1), --Wed
		(5, 999, 4, 59.99, 1), --Thurs
		(6, 999, 5, 99.99, 2), --Fri
		(7, 999, 6, 99.99, 1)  --Sat
		) AS source(DailyRateId, RateId, [Day], Price, MinimumStay)
		ON target.DailyRateId = source.DailyRateId
	WHEN MATCHED THEN
		UPDATE SET target.RateId = source.RateId, target.[Day] = source.[Day], target.Price = source.Price, target.MinimumStay = source.MinimumStay
	WHEN NOT MATCHED THEN
		INSERT (DailyRateId, RateId, [Day], Price, MinimumStay) VALUES (source.DailyRateId, source.RateId, source.[Day], source.Price, source.MinimumStay);

SET IDENTITY_INSERT DailyRates OFF
