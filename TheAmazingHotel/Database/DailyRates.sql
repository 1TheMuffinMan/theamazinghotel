﻿CREATE TABLE [dbo].[DailyRates]
(
	[DailyRateId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [RateId] INT NOT NULL, 
    [Day] INT NOT NULL, 
    [Price] DECIMAL(7, 2) NOT NULL, 
    [MinimumStay] INT NULL DEFAULT 1, 
    CONSTRAINT [CK_DailyRates_Day] CHECK ([Day] >= 0 AND [Day] <= 6), 
    CONSTRAINT [CK_DailyRates_MinimumStay] CHECK (MinimumStay > 0), 
    CONSTRAINT [FK_DailyRates_Rates] FOREIGN KEY ([RateId]) REFERENCES [Rates]([RateId])
)
