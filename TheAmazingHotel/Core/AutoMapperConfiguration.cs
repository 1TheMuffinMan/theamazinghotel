﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.Entities;

namespace Core
{
    public class AutoMapperConfiguration
    {
        public static IMapper Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new RateProfile());
            });

            config.AssertConfigurationIsValid();
            return config.CreateMapper();
        }

        public class RateProfile : Profile
        {
            public RateProfile()
            {
                CreateMap<RateWithDailyRatesEntity, Rate>()
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Rate.RateId))
                    .ForMember(d => d.Name, m => m.MapFrom(s => s.Rate.Name))
                    .ForMember(d => d.Rates, m => m.Ignore());
            }
        }
    }
}
