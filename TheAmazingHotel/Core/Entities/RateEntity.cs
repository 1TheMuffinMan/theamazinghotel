﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncPoco;

namespace Core.Entities
{
    [TableName("Rates")]
    [PrimaryKey("RateId")]
    public class RateEntity
    {
        public int RateId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
