﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncPoco;

namespace Core.Entities
{
    [TableName("DailyRate")]
    [PrimaryKey("DailyRateId")]
    public class DailyRateEntity
    {
        public int DailyRateId { get; set; }
        public int RateId { get; set; }
        public int Day { get; set; }
        public decimal Price { get; set; }
        public int MinimumStay { get; set; }
    }
}
