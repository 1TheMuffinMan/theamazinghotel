﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class RateWithDailyRatesEntity
    {
        public RateEntity Rate { get; set; }
        public List<DailyRateEntity> DailyRates { get; set; } = new List<DailyRateEntity>(); 
    }
}
