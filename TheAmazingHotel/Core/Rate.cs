﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core
{
    public class Rate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<DailyRate> Rates { get; set; } = new List<DailyRate>();
    }

    public class DailyRate
    {
        public DateTime Date { get; set; }
        public int MinTotalStayInDays { get; set; }
        public decimal Price { get; set; }
    }
}
