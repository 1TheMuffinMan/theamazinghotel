﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AsyncPoco;
using Core.Entities;
using IMapper = AutoMapper.IMapper;

namespace Core.Repository
{
    public class RatesRepository : IRatesRepository
    {
        private readonly IDbContext _db;
        private readonly IMapper _mapper;

        public RatesRepository(IDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<Rate> GetRoomRatesById(int id, DateTime startDate, DateTime endDate)
        {
            if (startDate >= endDate)
                throw new ArgumentOutOfRangeException(nameof(startDate) + " has to come before " + nameof(endDate));

            var sql = Sql.Builder
                .Select("r.*, dr.*")
                .From("Rates r")
                .LeftJoin("DailyRates dr")
                    .On("r.RateId = dr.RateId")
                .Where("r.RateId = @id", new { id })
                .Where("@startDate >= r.StartDate AND @endDate <= r.EndDate", new
                {
                    startDate,
                    endDate
                })
                .OrderBy("dr.Day");

            RateWithDailyRatesEntity result = null;
            using (var db = await _db.GetDatabaseAsync())
            {
                //do not use raw output of query, as we'll specify the mapping in the callback for each row returned
                await db.FetchAsync<RateEntity, DailyRateEntity, RateWithDailyRatesEntity>(
                     (rate, dailyRate) =>
                     {
                         if (result == null)
                             result = new RateWithDailyRatesEntity();

                         result.Rate = rate;
                         //0 signifies null 
                         if (dailyRate.DailyRateId != 0)
                             result.DailyRates.Add(dailyRate);

                         return result;
                     }, sql);
            }

            if (result == null)
                return null;

            //map internal entities to a public dto
            var rateDto = _mapper.Map<RateWithDailyRatesEntity, Rate>(result);
            //expand the internal concept of the same weekly rates to a flat list of rates by date
            rateDto.Rates = ExpandRatesToSchedule(result.DailyRates, startDate, endDate);

            return rateDto;
        }

        private IEnumerable<DailyRate> ExpandRatesToSchedule(IEnumerable<DailyRateEntity> weeklyRateSet, DateTime start, DateTime end)
        {
            var dailyRatesList = weeklyRateSet.ToList();
            for (var day = start.Date; day.Date <= end.Date; day = day.AddDays(1))
            {
                var matching = dailyRatesList[(int)day.DayOfWeek];
                yield return new DailyRate
                {
                    Date = day,
                    MinTotalStayInDays = matching.MinimumStay,
                    Price = matching.Price
                };
            }
        }
    }
}
