﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Repository
{
    public interface IRatesRepository
    {
        Task<Rate> GetRoomRatesById(int id, DateTime startDate, DateTime endDate);
    }
}
