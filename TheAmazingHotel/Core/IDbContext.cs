﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncPoco;

namespace Core
{
    public interface IDbContext
    {
        Task<IDatabase> GetDatabaseAsync();
    }
}
